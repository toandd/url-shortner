import json
import requests

# print (json.dumps(request, indent=4, sort_keys=True))
# Messages to run tests
links = {
    "original_url": "https://github.com/toandd-viosoft"
}

def shortener_url_request(data):
    headers = {
        'Content-Type': 'application/json',
    }

    return requests.post('http://localhost:9001/',
                         headers=headers, data=data)


# Generate a request to cancel the tests
def enter_short_url(short_url):
    # headers = {
    #     'Content-Type': 'application/json',
    # }

    return requests.get('http://localhost:9001/{}'.format(short_url))



if __name__ == '__main__':

    data = links
    short_url = '0'
    print shortener_url_request(json.dumps(data)).text
    print enter_short_url(short_url).text


