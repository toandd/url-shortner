from flask import redirect
from pymodm import connect
import logging
from flask import request
from http_status import app, response_201
from data_model import Data_Model

from convertion import id_to_shorten_string, shortened_string_to_id

global_id = 0
host = 'http://localhost:9001/'


database_address = 'mongodb://10.76.241.112:28017/evo'


def connect_db():
    try:
        connect(database_address)
        logging.info("Connected to {}".format(database_address))
    except:
        raise RuntimeError('Can not connect to {}'.format(database_address))


@app.route('/', methods=['POST'])
def home_page_access():
    global  global_id
    req_message = request.json
    # request.json
    # {
    #    original_url: ''
    # }
    new_id = global_id
    global_id = global_id + 1
    destination = req_message.get('original_url')
    encoded_string = id_to_shorten_string(new_id)
    Data_Model(
        id = new_id,
        original_url = destination
    ).save()
    req_message["short_string"] = encoded_string
    return response_201(req_message)


@app.route('/<short_url>')
def redirect_short_url(short_url):
    decoded_id = shortened_string_to_id(short_url)
    url = host
    for s in Data_Model.objects.all():
        if int(s.id) == decoded_id:
            url = s.original_url
            print(url)
    return redirect(url, code=307)


if __name__ == '__main__':
    # Connect to mongodb
    connect_db()
    app.run(debug=True,port=9001)