I: Pre-requirements

    1. Mongo database
    2. Python dependency packages

II: To run application, use following command

    1. cd url-shortener
    2. python main.py
    
III: To run test

    1. cd url-shortener
    2. python send_request.py
    
Url id will be started from 0