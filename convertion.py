# Convert unique id to a string, which will be send back to users

import string

possible_char = string.digits + string.lowercase + string.uppercase
num_allow_char = len(possible_char)


def id_to_shorten_string(id):
    unit_id = id % num_allow_char
    # Convert unit_id to a character in possible_char string
    generate_str = possible_char[unit_id]
    p = (id - unit_id) / num_allow_char
    while p:
        unit_id = p % num_allow_char
        p = (p - unit_id) / num_allow_char
        generate_str = possible_char[unit_id] + generate_str
    return generate_str


def shortened_string_to_id(shortened_string):
    generate_id = 0
    for i in xrange(len(shortened_string)):
        generate_id = num_allow_char * generate_id + possible_char.find(shortened_string[i])
    return generate_id

print shortened_string_to_id('1')